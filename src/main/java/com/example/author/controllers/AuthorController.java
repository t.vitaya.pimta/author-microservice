package com.example.author.controllers;

import com.example.author.dto.ResponseObj;
import com.example.author.exception.EntityNotFound;
import com.example.author.models.Author;
import com.example.author.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthorController {

    @Autowired
    AuthorService authorService;

    @GetMapping("/{authorId}")
    public ResponseEntity<ResponseObj<Author>> getAuthor(@PathVariable String authorId) {
        try {
            Author author = this.authorService.getAuthor(authorId);
            return ResponseEntity.ok(new ResponseObj<>(200, "OK", author));
        }
        catch (EntityNotFound e) {
            return ResponseEntity.ok(new ResponseObj<>(2000, e.getMessage()));
        }
    }
}
