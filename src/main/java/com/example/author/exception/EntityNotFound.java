package com.example.author.exception;

public class EntityNotFound extends Exception{
    public EntityNotFound(String errMessage) {
        super(errMessage);
    }
}
