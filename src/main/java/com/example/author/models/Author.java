package com.example.author.models;

import lombok.Getter;

import java.sql.Timestamp;

@Getter
public class Author {
    private String uuid;
    private String name;
    private Timestamp createdTime;
    private Timestamp updatedTime;

    public Author(String uuid, String name, Timestamp createdTime, Timestamp updatedTime) {
        this.uuid = uuid;
        this.name = name;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }
}
