package com.example.author.services;

import com.example.author.exception.EntityNotFound;
import com.example.author.models.Author;
import com.example.author.repository.JdbcAuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

@Component
public class AuthorService {
    @Autowired
    JdbcAuthorRepository authorRepository;

    public Author getAuthor(String authorId) throws EntityNotFound {
        try {
            return this.authorRepository.getById(authorId);
        }
        catch (DataAccessException e) {
            throw new EntityNotFound("author not found");
        }
    }
}
