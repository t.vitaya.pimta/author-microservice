package com.example.author.repository;

import com.example.author.models.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class JdbcAuthorRepository implements AuthorRepository{
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Author getById(String id) {
        return jdbcTemplate.queryForObject(
                "select * from author where uuid = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        new Author(
                                rs.getString("uuid"),
                                rs.getString("name"),
                                rs.getTimestamp("createdTime"),
                                rs.getTimestamp("updatedTime")
                        )
        );
    }
}
