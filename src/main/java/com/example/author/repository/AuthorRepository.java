package com.example.author.repository;

import com.example.author.models.Author;

public interface AuthorRepository {

    Author getById(String id);
}
