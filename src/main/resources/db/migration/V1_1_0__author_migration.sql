CREATE TABLE IF NOT EXISTS `author` (
    `uuid` varchar(36) NOT NULL PRIMARY KEY,
    `name` varchar(255) NOT NULL,
    `createdTime` timestamp NOT NULL default current_timestamp,
    `updatedTime` timestamp NOT NULL default current_timestamp
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
